import { call } from 'redux-saga/effects';
import _ from 'lodash';
import { showErrorMsg } from '../utils/notification';
// import { logout } from './login/actions';

// const ERROR_CODE = [401];

export function makeConstantCreator(...params) {
  const constant = {};
  _.each(params, param => {
    constant[param] = param;
  });
  return constant;
}

export const makeActionCreator = (type, params = null) => ({ type, ...params });

export const makeReducerCreator = (initialState = null, handlers = {}) => (
  state = initialState,
  action
) => {
  if (!action && !action.type) return state;
  const handler = handlers[action.type];
  return (handler && handler(state, action)) || state;
};

export function* apiWrapper(options, apiFunc, ...params) {
  try {
    const response = yield call(apiFunc, ...params);
    return response;
  } catch (err) {
    showErrorMsg({ description: err && err.message });
    throw new Error(err);
  }
}
