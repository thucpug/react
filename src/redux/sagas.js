import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import giftSaga from './gift/sagas';

export default function* root() {
  yield all([...authSaga, ...giftSaga]);
}
