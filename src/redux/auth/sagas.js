import { takeEvery, put, call } from 'redux-saga/effects';
import { apiWrapper } from '../reduxCreator';
import { postAPI, getAPI } from '../../api';
import { AuthTypes, loginSuccessAction, resetAuthAction } from './actions';

function* loginSaga({ email, password }) {
  const data = yield call(apiWrapper, null, postAPI, '/auth/login', { email, password });
  if (data && data.token) localStorage.setItem('sessionToken', data.token);
  yield put(loginSuccessAction(data));
}

function* getCurrentUserSaga() {
  try {
    const data = yield call(apiWrapper, null, getAPI, '/users/me');
    if (data && data.token) localStorage.setItem('sessionToken', data.token);
    yield put(loginSuccessAction(data));
  } catch (ex) {
    localStorage.removeItem('sessionToken');
    yield put(resetAuthAction());
  }
}

export default [
  takeEvery(AuthTypes.LOGIN, loginSaga),
  takeEvery(AuthTypes.GET_CURRENT_USER, getCurrentUserSaga),
];
