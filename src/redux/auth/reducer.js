import { AuthTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';

export const initialState = {
  isAuthenticated: false,
  roles: '',
  loginError: false,
  loginSuccess: false,
};

const loginSuccess = state => ({
  ...state,
  isAuthenticated: true,
  loginError: false,
  loginSuccess: true,
});

const resetAuthen = () => ({
  ...initialState,
});

export const auth = makeReducerCreator(initialState, {
  [AuthTypes.LOGIN_AUTH_SUCCESS]: loginSuccess,
  [AuthTypes.RESET_AUTHEN]: resetAuthen,
});
