import { makeConstantCreator, makeActionCreator } from '../reduxCreator';

export const AuthTypes = makeConstantCreator(
  'LOGIN',
  'LOGIN_AUTH_SUCCESS',
  'GET_CURRENT_USER',
  'RESET_AUTHEN'
);

export const loginAction = ({ email, password }) =>
  makeActionCreator(AuthTypes.LOGIN, { email, password });
export const loginSuccessAction = () => makeActionCreator(AuthTypes.LOGIN_AUTH_SUCCESS);

export const getCurrentUserAction = () => makeActionCreator(AuthTypes.GET_CURRENT_USER);

export const resetAuthAction = () => makeActionCreator(AuthTypes.RESET_AUTHEN);
