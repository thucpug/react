import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { auth } from './auth/reducer';
import { gift } from './gift/reducer';

export default history =>
  combineReducers({
    router: connectRouter(history),
    auth,
    gift,
  });
