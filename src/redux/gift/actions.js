import { makeConstantCreator, makeActionCreator } from '../reduxCreator';

export const UserTypes = makeConstantCreator('SUBSCRIBE_USER', 'FETCH_LIST_USERS_SUCCESS');

export const subscribeUserAction = () => makeActionCreator(UserTypes.SUBSCRIBE_USER);
export const fetchListUserSuccessAction = data =>
  makeActionCreator(UserTypes.FETCH_LIST_USERS_SUCCESS, { data });
