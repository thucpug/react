import { UserTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';

export const initialState = {
  listUsers: [],
  listGifts: [],
  totalUsers: 0,
};

const fetchListUsersSuccess = (state, { data }) => ({
  ...state,
  listUsers: data.users,
  listGifts: data.gifts,
  totalUsers: data.userCount.count,
});

export const gift = makeReducerCreator(initialState, {
  [UserTypes.FETCH_LIST_USERS_SUCCESS]: fetchListUsersSuccess,
});
