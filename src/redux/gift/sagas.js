import { takeEvery, take, put, call } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { UserTypes, fetchListUserSuccessAction } from './actions';
import { initSocket } from '../../api/socket';

function userSubscribe(socket) {
  return eventChannel(emit => {
    const update = data => emit(data);
    socket.on('connected', update);
    socket.on('changed', update);
    return () => {
      socket.close();
    };
  });
}

function* watchListUserSaga() {
  const socket = initSocket();
  const channel = yield call(userSubscribe, socket);
  while (true) {
    const data = yield take(channel);
    yield put(fetchListUserSuccessAction(data));
  }
}

export default [takeEvery(UserTypes.SUBSCRIBE_USER, watchListUserSaga)];
