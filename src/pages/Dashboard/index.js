import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout } from 'antd';
import DashboardWrapper from './styles';
import { disconnectSocket } from '../../api/socket';
// import images
import LogoWhite from '../../assets/images/logoWhite.png';

// import containers
import ListWinners from '../../containers/ListWinners';
import TotalUsersSection from '../../containers/TotalUsersSection';
import GiftInventory from '../../containers/GiftInventory';
import PrivateLayout from '../../layout/PrivateLayout';

// import actions
import { subscribeUserAction } from '../../redux/gift/actions';

const { Footer, Content } = Layout;

const Dashboard = ({ subscribeUser }) => {
  useEffect(() => {
    subscribeUser();
    return () => {
      disconnectSocket();
    };
  }, []);

  return (
    <PrivateLayout>
      <DashboardWrapper>
        <Layout>
          <Content>
            <div className="header-div container">
              <div className="total-number-div">
                <TotalUsersSection />
              </div>
              <GiftInventory />
            </div>
            <ListWinners />
          </Content>
          <Footer className="footer">
            <img src={LogoWhite} alt="Enouvo" />
          </Footer>
        </Layout>
      </DashboardWrapper>
    </PrivateLayout>
  );
};

Dashboard.propTypes = {
  subscribeUser: PropTypes.func,
};

export default connect(
  () => ({}),
  dispatch => ({
    subscribeUser: () => {
      dispatch(subscribeUserAction());
    },
  })
)(Dashboard);
