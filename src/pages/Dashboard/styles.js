import styled from 'styled-components';
import theme from '../../configs/theme';

export default styled.div`
  .ant-layout {
    background-color: ${theme.palette.primaryColor};
    color: white;
    height: 100vh;
  }
  .container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }

  .footer {
    border-top: silver solid thin;
    text-align: center;
    margin-top: 50px;

    img {
      width: 100px;
    }
  }

  .header-div {
    display: flex;
    margin-top: 40px;
    margin-bottom: 60px;

    .total-number-div {
      flex: 1;
    }
    .list-gifts {
      display: flex;
      flex: 4;
      justify-content: space-around;
    }
  }

  .gift-icon {
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    border-radius: 50%;
    justify-content: center;
    width: 100px;
    height: 100px;

    & > div {
      text-align: center;
      img {
        width: auto;
        height: 50px;
      }
    }

    .text {
      color: rgba(0, 0, 0, 0.65);
      margin-top: 5px;
      font-size: 12px;

      strong {
        color: black;
        font-size: 14px;
      }
    }
  }

  .ant-table-empty {
    .ant-table-placeholder {
      background-color: ${theme.palette.primaryColor};
      border: none !important;
    }
  }

  @media (min-width: 576px) {
    .container {
      max-width: 540px;
    }
  }

  @media (min-width: 768px) {
    .container {
      max-width: 720px;
    }
  }

  @media (min-width: 992px) {
    .container {
      max-width: 960px;
    }
  }

  @media (min-width: 1200px) {
    .container {
      max-width: 1140px;
    }
  }
`;
