import React from 'react';
import { Layout } from 'antd';
import HomeWrapper from './styles';
// import components
import Banner from '../../components/Banner';

const { Content } = Layout;

const Home = () => (
  <HomeWrapper>
    <Layout>
      <Content>
        <Banner />
      </Content>
    </Layout>
  </HomeWrapper>
);

export default Home;
