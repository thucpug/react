import styled from 'styled-components';
import background from '../../assets/images/background.png';

export default styled.div`
  .container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }

  .footer {
    border-top: silver solid thin;
    text-align: center;
    margin-top: 20px;
    color: white;
  }

  .ant-layout {
    min-height: 100vh;
    background-image: url(${background});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  .container {
    padding: 0 120px;
  }
`;
