import openSocket from 'socket.io-client';

// eslint-disable-next-line
let socket = null;
export const initSocket = () => {
  const token = localStorage.getItem('sessionToken');
  if (!token) return null;
  socket = openSocket(`${process.env.REACT_APP_SERVER_URL}/gifts`, {
    query: `token=${token}`,
    transports: ['websocket'],
  });
  socket.on('connected', () => {
    console.log('Socket connected!!!!');
  });
  return socket;
};

export const disconnectSocket = () => {
  if (socket) socket.disconnect();
};

export default socket;
