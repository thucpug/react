import React from 'react';
import { Avatar } from 'antd';
import HeaderWrapper from './styles';

// import assets
import logo from '../../assets/images/logo-avatar.png';

const Header = () => (
  <HeaderWrapper>
    <Avatar src={logo} />
  </HeaderWrapper>
);

export default Header;
