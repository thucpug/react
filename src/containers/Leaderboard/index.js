import React from 'react';
import { Card, Icon } from 'antd';
import LeaderBoardWrapper from './styles';

// Init variables
const { Meta } = Card;

const LeaderBoard = () => (
  <LeaderBoardWrapper>
    {Array(...Array(3)).map(() => (
      <Card style={{ width: 250 }} actions={[<Icon type="setting" />, <Icon type="edit" />]}>
        <Meta
          avatar={(
            <img
              src="https://res.cloudinary.com/quanghd0102/image/upload/c_scale,fl_progressive,w_100/v1552489738/49950012_102340367530495_8864302875304525824_n.jpg"
              alt=""
            />
)}
          title="Hoang Ho"
          description="Cu-đơ"
        />
      </Card>
    ))}
  </LeaderBoardWrapper>
);

export default LeaderBoard;
