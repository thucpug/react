import styled from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: space-around;

  .ant-card-meta-avatar {
    width: 100%;
    float: none;
    text-align: center;
    padding: 0;

    img {
      width: 100px;
      border-radius: 50%;
      margin: 10px 0 15px;
    }
  }

  .ant-card-meta-detail {
    text-align: center;
  }
`;
