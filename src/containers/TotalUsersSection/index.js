import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TotalUsersSectionWrapper from './styles';
// import components
import TotalUser from '../../components/TotalUser';

const TotalUsersSection = ({ totalUsers }) => (
  <TotalUsersSectionWrapper>
    <div className="total-usesr">
      <div className="text">
        <span>Số người tham gia</span>
      </div>
      <div className="number">
        <TotalUser number={totalUsers || 0} />
      </div>
    </div>
  </TotalUsersSectionWrapper>
);

TotalUsersSection.propTypes = {
  totalUsers: PropTypes.number,
};

export default connect(state => ({
  totalUsers: state.gift.totalUsers,
}))(TotalUsersSection);
