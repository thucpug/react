import styled from 'styled-components';
import theme from '../../configs/theme';

export default styled.div`
  text-align: center;

  .total-usesr {
    .text {
      font-weight: 600;
      font-size: 24px;
    }
  }
  .download-btn {
    margin-left: 30px;
    position: relative;
    width: 157px;
    margin: auto;

    button {
      border-radius: 0;
      background-color: ${theme.palette.primaryColor};
      color: white;
      font-size: 18px;
      line-height: 28px;
      font-weight: bold;
      padding: 5px 15px;
      height: auto;
      z-index: 2;
    }

    img {
      width: 157px;
      position: absolute;
      top: 5px;
      left: 5px;
    }
  }
`;
