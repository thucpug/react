import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Avatar } from 'antd';
import { format } from 'date-fns';
import ListWinnersWrapper from './styles';

// Init variables
const columns = [
  {
    title: 'Tên',
    dataIndex: 'fullName',
    key: 'fullName',
    render: (text, record) => {
      if (record.avatar)
        return (
          <div className="align-center-div">
            <Avatar size="large" src={record.avatar} />
            <span style={{ marginLeft: 5 }}>{text}</span>
          </div>
        );

      return (
        <div className="align-center-div">
          <Avatar size="large" icon="user" />
          <span style={{ marginLeft: 5 }}>{text}</span>
        </div>
      );
    },
  },
  {
    title: 'Phần quà',
    dataIndex: 'giftName.vi',
    key: 'giftName.vi',
  },
  {
    title: 'Thời gian',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: text => format(new Date(text), 'HH:mm:ss'),
  },
];

const ListWinners = ({ listUsers }) => (
  <ListWinnersWrapper className="container">
    <div className="title">Danh Sách Người Trúng Thưởng</div>
    <Table dataSource={listUsers} columns={columns} showHeader={false} />
  </ListWinnersWrapper>
);

ListWinners.propTypes = {
  listUsers: PropTypes.array,
};

export default connect(state => ({
  listUsers: state.gift.listUsers,
}))(ListWinners);
