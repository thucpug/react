import styled from 'styled-components';

export default styled.div`
  text-align: center;

  .title {
    font-size: 25px;
    font-weight: bold;
    margin-bottom: 25px;
    text-transform: uppercase;
  }

  .ant-table-placeholder {
    border: none;
  }

  .ant-table {
    color: white;

    .ant-table-row {
      background-color: #ff5151 !important;
      font-size: 20px;
    }
  }

  .align-center-div {
    display: flex;
    align-items: center;
  }

  .ant-pagination {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    margin-top: 35px;
  }
  .ant-pagination-total-text {
    display: flex;
    align-items: center;
  }
  .ant-pagination-options-quick-jumper {
    font-weight: 300;
    font-size: 14px;
  }
  .ant-select-selection-selected-value {
    font-weight: 300;
    font-size: 14px;
  }
  .ant-pagination-item {
    font-weight: 300;
    font-size: 25px;
    background: transparent;
    border: none;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    a {
      color: white !important;
      font-weight: 300;
      font-size: 25px;
      &:hover {
        color: white !important;
      }
    }
    &:hover {
      border: 1px solid white !important;
    }
  }
  .ant-pagination-item-active {
    background: transparent;
    border: 1px solid white !important;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    color: white !important;
    display: flex;
    align-items: center;
    justify-content: center;
    a {
      color: white !important;
      font-weight: 300;
      font-size: 25px;
      &:hover {
        color: white !important;
      }
    }
  }

  .ant-pagination-item-link {
    border: none !important;
    background: transparent !important;
    color: white !important;
  }
`;
