import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import images
import tShirtIcon from '../../assets/images/t-shirt.png';
import bottleIcon from '../../assets/images/bottle.png';
import cupIcon from '../../assets/images/cup.png';
import capIcon from '../../assets/images/cap-min.png';
import voucherIcon from '../../assets/images/icVoucher-min.png';
// import keychainIcon from '../../assets/images/keychain.png';
// import handIcon from '../../assets/images/hand.png';
// import pencilIcon from '../../assets/images/pencil.png';
import documentIcon from '../../assets/images/document.png';

const listGiftIcons = [
  tShirtIcon,
  cupIcon,
  voucherIcon,
  bottleIcon,
  capIcon,
  // keychainIcon,
  // handIcon,
  documentIcon
  // pencilIcon,
];

const GiftInventory = ({ listGifts }) => (
  <div className="list-gifts">
    {listGifts.map(gift => (
      <div className="gift-icon">
        <div>
          <img src={listGiftIcons[gift.id - 1]} alt="" />
        </div>
        <div className="text">
          còn lại
          <strong> {gift.quantity}</strong>
        </div>
      </div>
    ))}
  </div>
);

GiftInventory.propTypes = {
  listGifts: PropTypes.array
};

export default connect(state => ({
  listGifts: state.gift.listGifts
}))(GiftInventory);
