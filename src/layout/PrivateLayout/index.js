import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PrivateLayoutWrapper from './styles';

// Import actions
import { getCurrentUserAction } from '../../redux/auth/actions';

const PrivateLayout = ({ children, isAuthenticated, getCurrentUser }) => {
  if (
    !isAuthenticated &&
    !localStorage.getItem('sessionToken') &&
    window.location.pathname !== '/login'
  )
    return <Redirect to="/login" />;
  if (!isAuthenticated && localStorage.getItem('sessionToken')) {
    getCurrentUser();
    return null;
  }

  return <PrivateLayoutWrapper>{children}</PrivateLayoutWrapper>;
};
PrivateLayout.propTypes = {
  children: PropTypes.any,
  isAuthenticated: PropTypes.bool,
  getCurrentUser: PropTypes.func,
};

export default connect(
  state => ({
    isAuthenticated: state.auth.isAuthenticated,
  }),
  dispatch => ({
    getCurrentUser: () => dispatch(getCurrentUserAction()),
  })
)(PrivateLayout);
