import styled from 'styled-components';

const PublicLayoutWrapper = styled.div`
  .layout {
    height: 100vh;
  }

  .main-img {
    background-image: url(https://res.cloudinary.com/csmenouvo/image/upload/v1550032735/background/login_background.jpg);
    background-color: transparent;
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
  }

  .main-content {
    background-color: white;
    padding: 70px 50px;
    text-align: center;
  }
`;

export default PublicLayoutWrapper;
