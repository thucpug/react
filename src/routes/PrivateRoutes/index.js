import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { flatMap, map } from 'lodash';
import Dashboard from '../../pages/Dashboard';

const routes = [
  {
    path: '/dashboard',
    component: Dashboard,
    exact: true,
  },
];

const PrivateRoutes = () => (
  <Switch>
    {map(
      flatMap(routes, route => {
        if (route.routes) {
          return map(route.routes, subRoute => ({
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/',
          }));
        }
        return route;
      }),
      route => (
        <Route {...route} key={route.path} />
      )
    )}
  </Switch>
);

PrivateRoutes.propTypes = {};

export default PrivateRoutes;
