import React from 'react';
import { Switch, Route } from 'react-router-dom';
// import pages
import Login from '../../pages/Login';
import Home from '../../pages/Home';

const routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/login',
    component: Login,
  },
];

const PublicRoutes = () => (
  <Switch>
    {routes.map(route => (
      <Route {...route} key={route.path} />
    ))}
  </Switch>
);

export default PublicRoutes;
