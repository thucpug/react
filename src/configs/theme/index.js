const theme = {
  palette: {
    primaryColor: '#FF5151',
    secondaryColor: 'rgba(45, 48, 71, 1)',
    loadingBackgroundColor: '#2c3e51cc',
  },
  backgroundColor: '#FCFDFF',
  lineColor: '#E4E6E9',
  fonts: {
    primary: 'Georgia, serif',
  },
};

module.exports = theme;
