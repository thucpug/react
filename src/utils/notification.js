import { notification } from 'antd';

const { error, success } = notification;

export const showErrorMsg = ({ title = 'Error', description = 'Server Internal Error' }) => {
  error({
    message: title,
    description,
  });
};

export const showSuccessMsg = ({ title = 'Success', description = '' }) => {
  success({
    message: title,
    description,
  });
};
