import styled from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: center;

  .border-div {
    width: 50px;
    height: 50px;
    box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.12);
    border-radius: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 38px;
    font-weight: bold;
    margin: 8px 10px;
    background: white;
    color: #ff5151;
  }
`;
