import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';
import TotalUserWrapper from './styles';

const TotalUser = ({ number }) => {
  const numberString = String(number);

  return (
    <TotalUserWrapper>
      <div className="border-div">0</div>
      {map(numberString, (num, index) => (
        <div className="border-div" key={index}>
          {num}
        </div>
      ))}
    </TotalUserWrapper>
  );
};

TotalUser.propTypes = {
  number: PropTypes.number.isRequired,
};

export default TotalUser;
