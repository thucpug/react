import styled from 'styled-components';

export default styled.div`
  img {
    width: 200px;
  }

  .text {
    font-size: 25px;
    color: white;
    margin-top: 10px;
  }
`;
