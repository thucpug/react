import React from 'react';
import EmptyWrapper from './styles';
import giftIcon from '../../assets/images/gift.png';

const EmptyCustom = () => (
  <EmptyWrapper>
    <div>
      <img src={giftIcon} alt="" />
    </div>
    <div className="text">Chúng tôi vẫn đang chờ đợi những người chơi may mắn!</div>
  </EmptyWrapper>
);

export default EmptyCustom;
