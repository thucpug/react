import styled from 'styled-components';

export default styled.h1`
  font-size: 30px;
  width: 100%;
  margin-bottom: 30px;
  display: flex;
  align-items: center;
  white-space: nowrap;
  padding: 5px 10px;

  @media only screen and (max-width: 767px) {
    margin: 0 10px;
    margin-bottom: 30px;
  }

  i {
    color: #faad14;
    font-size: 14px;
    margin-right: 5px;
  }
`;
