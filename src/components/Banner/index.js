import React from 'react';
import { Row, Col, Button } from 'antd';
import BannerWrapper from './styles';
// import images
import Logo from '../../assets/images/Logo.jpg';
import QRCode from '../../assets/images/qr.png';
import shabowBtn from '../../assets/images/CTA.jpg';
import GiftBanner from '../../assets/images/gift-banner.png';

const Banner = () => (
  <BannerWrapper>
    <div className="container">
      <Row className="banner-div">
        <Col lg={10} md={24}>
          <div className="logo-div">
            <img src={Logo} alt="Enouvo" />
          </div>
          <div className="title-div">
            <div>Chào mừng bạn đến với</div>
            <div>
              Tải App Ngay
              <br />
              Quà Liền Tay!
            </div>
            <div>
              Enovents đem đến cho người dùng cái nhìn tổng quát nhất về các sự kiện được tổ chức và
              tham gia bởi các thành viên Enouvo. Tải ngay ứng dụng để có cơ hội tham gia các trò
              chơi và mang về những phần quà hấp dẫn!
            </div>
          </div>
          <div className="download-div">
            <div className="download-btn">
              <a href="https://app.enouvo.com" target="_blank" rel="noopener noreferrer">
                <Button>Download App</Button>
              </a>
              <img src={shabowBtn} alt="" />
            </div>
            <div>
              <img src={QRCode} alt="" />
            </div>
          </div>
        </Col>
        <Col lg={14} md={0} sm={0} xs={0} className="illustraion-div">
          <br />
          <img src={GiftBanner} alt="" />
        </Col>
      </Row>
    </div>
  </BannerWrapper>
);

export default Banner;
