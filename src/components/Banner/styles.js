import styled from 'styled-components';
import theme from '../../configs/theme';

export default styled.div`
  .banner-div {
    margin-bottom: 30px;
    .logo-div {
      margin-top: 60px;
      margin-bottom: 80px;
    }

    .title-div {
      & > div:first-child {
        font-size: 24px;
        color: #9b9b9b;
      }

      & > div:nth-child(2) {
        margin: 25px 0;
        font-size: 38px;
        line-height: 41px;
        color: #c1351f;
        font-weight: bold;
      }

      & > div:nth-child(3) {
        color: #4a4a4a;
        font-size: 16px;
      }
    }

    .download-div {
      margin-top: 30px;
      display: flex;
      flex-direction: column;

      img {
        width: 150px;
      }

      .download-btn {
        margin-bottom: 30px;
        position: relative;

        button {
          border-radius: 0;
          background-color: ${theme.palette.primaryColor};
          color: white;
          font-size: 18px;
          line-height: 28px;
          font-weight: bold;
          padding: 5px 15px;
          height: auto;
          z-index: 2;
        }

        img {
          width: 157px;
          position: absolute;
          top: 5px;
          left: 5px;
        }
      }
    }

    .illustraion-div {
      img {
        float: right;
        width: 80%;
      }
    }
  }
`;
