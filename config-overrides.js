const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const themeConfig = require('./src/configs/theme');

/* eslint-disable */
module.exports = function override(config, env) {
  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
    config
  );
  config = rewireLess.withLoaderOptions({
    modifyVars: {
      '@primary-color': themeConfig.palette.primaryColor,
      '@layout-body-background': themeConfig.backgroundColor,
      '@layout-header-background': themeConfig.backgroundColor,
      '@table-row-hover-bg': themeConfig.palette.primaryColor,
      '@table-border-radius-base': '20px',
      '@layout-footer-background': themeConfig.palette.primaryColor,
    },
    javascriptEnabled: true,
  })(config, env);
  return config;
};
